## 201813 CS95063

---

**Repositorio de la clase Gráficas Computacionales Interactivas (Tópico de desarrollo de videojuegos), semestre Ago-Dic 2018.**
*Utilizar Unity3D 2018.2.6f1*

---

Grupo 1
Viernes 16:00 a 19:00 hrs

---

Proyecto de Unity Technologies que combina navegación utilizando NavMesh y un personaje en primera persona (Standard Assets de Unity).
[Playlist de  Youtube](https://www.youtube.com/watch?v=ZigSuoGmJ0Y&index=1&list=PLX2vGYjWbI0Rf0im34I2lBF4eufM-cgzQ)